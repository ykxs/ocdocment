//
//  ViewController.swift
//  FirstSwiftCoreData
//
//  Created by ykxs on 2019/9/10.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit

let ScreenWidth = UIScreen.main.bounds.size.width
let ScreenHeight = UIScreen.main.bounds.size.height

class ViewController: UIViewController {

    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var ageTF: UITextField!
    @IBOutlet weak var heightTF: UITextField!
    @IBOutlet weak var sexTF: UITextField!
    
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var reloadBtn: UIButton!
    
//    var headerView = UIView.init()
    
    var dataArr:[Student] = [Student]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
//        headerView.frame = CGRect(x: 0, y: 0, width: ScreenWidth, height: 120)
//        headerView.addSubview(nameTF)
//        headerView.addSubview(ageTF)
//        headerView.addSubview(nameTF)
//        headerView.addSubview(sexTF)
//        headerView.addSubview(addBtn)
//        headerView.addSubview(reloadBtn)
//        headerView.backgroundColor = UIColor.red
//        self.tableview.tableHeaderView = headerView
        
        nameTF.text = "li "
        ageTF.text = "14"
        heightTF.text = "155.3"
        sexTF.text = "1"
        self.view.addSubview(self.tableview)
        
        let model = WorkerModel.init("hua", 18, false)
        Table_Name = "create_name"
        let manager = UserDBManager.sharedInstance
//        print(manager.isExisitTable())
        manager.createTable()
//        manager.addModelData(model: model)
//        manager.queryModelData("") {
//            print("list:\($0.count)")
//        }
//        manager.deleteCreateByTableName(Table_Name)
        manager.queryModelData("") {
            print("list:\($0.count)")
        }
    }
    
    lazy var tableview:UITableView = {
//        let tableview = UITableView(frame: CGRect.init(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight), style: .plain)
        let tableview = UITableView(frame: CGRect(x: 0, y: addBtn.frame.maxY + 20, width: ScreenWidth, height: ScreenHeight - addBtn.frame.maxY - 20), style: .plain)
        tableview.delegate = self
        tableview.dataSource = self
        tableview.rowHeight = 50
        //自适应宽度
        tableview.register(UINib(nibName: "CoreDataTBCell", bundle: nil), forCellReuseIdentifier: "cell")
        return tableview
    }()

    @IBAction func addBtnDataDone(_ sender: Any) {
        if nameTF.text != "" && nameTF.text!.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 && ageTF.text!.count > 0 && ageTF.text!.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 && heightTF.text!.count > 0 && heightTF.text!.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 && sexTF.text!.count > 0 && sexTF.text!.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 {
            guard let nameString = nameTF.text else { return }
            guard let ageString = ageTF.text else { return }
            guard let heightString = heightTF.text else { return }
            guard let sexString = sexTF.text else { return }
            guard let age = Int32(ageString) else { return }
            guard let height = Float(heightString) else { return }
            let sex = sexString == "0" ? false : true
            CoreDataManager.shared.addStudentWith(nameString, age, height, sex)
        }
    }
    
    @IBAction func reloadBtnDataDone(_ sender: UIButton) {
        
    }
    
    func reloadTableviewData() {
        dataArr = CoreDataManager.shared.getAllStudentData()
        tableview.reloadData()
    }
    
}

extension ViewController:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CoreDataTBCell
        let model = dataArr[indexPath.row]
        cell.model = model
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let cell = tableView.cellForRow(at: indexPath)
//        let name = cell?.textLabel?.text?.components(separatedBy: "\n")
//        CoreDataManager.shared.deleteWith(name![0])
////        dataArr = CoreDataManager.shared.getAllStudentData()
//        tableView.reloadData()
    }
}
