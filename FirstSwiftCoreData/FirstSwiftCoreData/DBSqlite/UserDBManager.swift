//
//  UserDBManager.swift
//  FirstSwiftCoreData
//
//  Created by ykxs on 2019/9/10.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit
import FMDB

var Table_Name = "draft_table"

class UserDBManager: NSObject {
    
    var userId:NSInteger = 0
    var localDb:FMDatabase?
    var queue:FMDatabaseQueue?
    
    static let sharedInstance = UserDBManager()
    
    func createTable() {
        let fileName = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first?.appending("\(Table_Name).sqlite")
        self.queue = FMDatabaseQueue.init(path: fileName)
        queue?.inDatabase({ (db) in
            if db.open() {
//                let isSuccess = db.executeUpdate("create table if not exists t_FirstSwiftCoreData (id integer primary key autoincrement,image blob)", withArgumentsIn: [])
                let isSuccess = db.executeUpdate("create table if not exists " + Table_Name + " (id integer primary key autoincrement, name text, age integer, sex integer);", withArgumentsIn: [])
                if isSuccess {
                    print("建表成功")
                } else {
                    print("建表失败")
                }
            }
            db.close()
        })
    }
    
    func addModelData(model:WorkerModel) {
        queue?.inDatabase({ (db) in
            if db.open() {
                print("modoeo:\(model.name ?? ""):\(model.age):\(model.sex)")
                let isSuccess = db.executeUpdate("insert into " + Table_Name + " (name, age, sex) values (?, ?, ?);", withArgumentsIn: [model.name!,model.age,model.sex]) //db.executeUpdate("insert into DataModel values(null,'%@','%@')",model.title,model.url, withArgumentsIn: [])
                if isSuccess {
                    print("插入成功")
                } else {
                    print("插入失败")
                }
                db.close()
            }
        })
//        return true
    }
    
    //删除数据
    func deleteModelData(model:WorkerModel) {
        queue?.inDatabase({ (db) in
            if db.open() {
                let isSuccess = db.executeUpdate("delete from " + Table_Name + " where name = '%d';", withArgumentsIn: [model.name ?? ""]) //db.executeUpdate("delete from t_FirstSwiftCoreData", withArgumentsIn: [])
                if isSuccess {
                    print("删除成功")
                } else {
                    print("删除失败")
                }
                db.close()
            }
        })
//        return true
    }
    
    func deleteModelDataByName(_ name:String) {
        queue?.inDatabase({ (db) in
            if db.open() {
                let isSuccess = db.executeUpdate("delete from " + Table_Name + " where name = '\(name)';", withArgumentsIn: [])
                if isSuccess {
                    print("删除成功")
                } else {
                    print("删除失败")
                }
                db.close()
            }
        })
        //        return true
    }
    //删除
    func deleteModelDataBySex(_ sex:Bool) {
        queue?.inDatabase({ (db) in
            if db.open() {
                let isSuccess = db.executeUpdate("delete from " + Table_Name + " where sex = '\(sex)';", withArgumentsIn: [])
                if isSuccess {
                    print("删除成功")
                } else {
                    print("删除失败")
                }
                db.close()
            }
        })
        //        return true
    }
    
    //删除表格
    func deleteCreateByTableName(_ tableName:String) {
        let sqlStr = "drop table if exists " + Table_Name
        queue?.inDatabase({ (db) in
            if db.open() {
                let isSuccess = db.executeUpdate(sqlStr, withArgumentsIn: [])
                if isSuccess {
                    print("删除表格成功")
                } else {
                    print("删除表格失败")
                }
            }
        })
    }
    
    
    
    //修改
    func updateModelData(_ name:String,_ model:NSObject) {
        queue?.inDatabase({ (db) in
            if db.open() {
//                db.executeStatements()
                let isSuccess = db.executeUpdate("update t_FirstSwiftCoreData set image = ?", withArgumentsIn: [])
                if isSuccess {
                    print("修改成功")
                } else {
                    print("修改失败")
                }
                db.close()
            }
        })
    }
    
    func isExisitTable() -> Bool {
        let sqlStr = "select count(*) as 'count' from sqlite_master where type ='table' and name = \(Table_Name);"
        
        var count:Int32 = 0
        
        self.queue?.inDatabase({ (db) in
            
            guard let res = db.executeQuery(sqlStr, withArgumentsIn: []) else { return }
            print("zoubba")
            while (res.next()) {
                count = res.int(forColumn: "count")
                print("count:\(count)")
            }
        })
        if count == 0 {
            return false
        }
        return true
    }
    //查询数据
    func queryModelData(_ name:String,completionHandler:@escaping (_ modelList: [WorkerModel]) -> ()) {
        queue?.inDatabase({ (db) in
            if db.open() {
                print("chaxue")
                
                let resultSet = db.executeQuery("select * from " + Table_Name + " ;", withArgumentsIn: [])
                
                
//                print("resultSet:\(resultSet):\(resultSet?.int(forColumn: <#T##String#>))")
                var modelArr = [WorkerModel]()
                
                while (resultSet?.next())! {
                    let nameStr = resultSet?.string(forColumn: "name")
                   
                    print("mmmnanse:\(nameStr):\(name):\(resultSet?.columnCount)")
                    if nameStr == name || name == "" {
                        let sexRes:Int32 = (resultSet?.int(forColumn: "sex"))!
                        let ageIndex:Int32 = (resultSet?.int(forColumn: "age"))!
//                        let age = Int("\(ageIndex)")
//                        print("agge:\(Int(ageIndex))")
                        var sex = true
                        if sexRes != 1 {
                            sex = false
                        }
                        let model = WorkerModel.init(nameStr!, Int(ageIndex), sex)
                        modelArr.append(model)
                    }
                }
                db.close()
                completionHandler(modelArr)
            } else {
                completionHandler([])
            }
        })
    }
}


class WorkerModel: NSObject {
    var name: String?
    var age: Int = 0
    var sex: Bool = true
    
    init(_ name:String,_ age:Int,_ sex:Bool) {
        super.init()
        
        self.name = name
        self.age = age
        self.sex = sex
    }
}
