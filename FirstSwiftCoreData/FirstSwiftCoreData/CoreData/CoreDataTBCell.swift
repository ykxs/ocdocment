//
//  CoreDataTBCell.swift
//  FirstSwiftCoreData
//
//  Created by ykxs on 2019/9/10.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit

class CoreDataTBCell: UITableViewCell {
    @IBOutlet weak var nameLa: UILabel!
    @IBOutlet weak var sexLa: UILabel!
    @IBOutlet weak var heightLa: UILabel!
    @IBOutlet weak var ageLa: UILabel!
    
    public var model: Student? {
        didSet {
            
            nameLa.text = "名字: \(self.model!.name ?? "")"
            sexLa.text = self.model!.sex == true ? "性别: 男" : "性别: 女"
            heightLa.text = "身高: \(self.model!.height)"
            ageLa.text = "年龄: \(self.model!.age)"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
