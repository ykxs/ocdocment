//
//  CoreDataManager.swift
//  FirstSwiftCoreData
//
//  Created by ykxs on 2019/9/10.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit
import CoreData

class CoreDataManager: NSObject {
    static let shared = CoreDataManager()
    lazy var context:NSManagedObjectContext = {
        let context = (UIApplication.shared.delegate as! AppDelegate).context
        return context
    }()
    
    //更新数据
    private func saveContext() {
        
        do {
            try context.save()
        } catch {
            let faError = error as NSError
            fatalError("Unresolved error \(faError), \(faError.userInfo)")
        }
    }
    //增加数据
    func addStudentWith(_ name:String,_ age:Int32 ,_ height:Float ,_ sex:Bool) {
        
        let student = NSEntityDescription.insertNewObject(forEntityName: "Student", into: context) as! Student
        
        student.name = name
        student.height = height
        student.sex = sex
        student.age = age
        
        saveContext()
    }
    
    //根据名字获取数据
    func getStudentWith(name: String) -> [Student] {
        let fetchRequest:NSFetchRequest = Student.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "name == %@", name)
        do {
            let result: [Student] = try context.fetch(fetchRequest)
            return result
        } catch {
            fatalError()
        }
    }
    
    func getAllStudentData() -> [Student] {
        let fetchRequest:NSFetchRequest = Student.fetchRequest()
        do {
            let result = try context.fetch(fetchRequest)
            return result
        } catch {
            fatalError()
        }
    }
    
    //根据姓名修改数据
    func changeStudentWith(name:String, newName:String,newAge:Int32) {
        let fetchRequest:NSFetchRequest = Student.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "name == %@", name)
        do {
            let result = try context.fetch(fetchRequest)
            for student in result {
                student.name = newName
                student.age = newAge
            }
        } catch {
            fatalError()
        }
        saveContext()
    }
    
    //根据姓名删除数据
    func deleteWith(_ name:String) {
        let fetchRequest:NSFetchRequest = Student.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "name == %@", name)
        do {
            let result = try context.fetch(fetchRequest)
            for student in result {
                context.delete(student)
            }
        } catch {
            fatalError()
        }
        saveContext()
    }
    //删除所有的数据
    func deleteAllStudentData() {
        let result = getAllStudentData()
        for student in result {
            context.delete(student)
        }
        saveContext()
    }
    
    //保存图片  没啥用
    func saveImageCoreData() {
        let imageData:Data = UIImage.pngData(UIImage(named: "image.png")!)()!
        let imageEntity = NSEntityDescription.entity(forEntityName: "ImageEntity", in: context)
        let image = ImageEntity(entity: imageEntity!, insertInto: context)
        image.imageData = imageData
        do {
            try context.save()
        } catch {
            let faError = error as NSError
            fatalError("Unresolved error \(faError), \(faError.userInfo)")
        }
        //获取图片
        let fetchRequest:NSFetchRequest = ImageEntity.fetchRequest()
        do {
            let result:[ImageEntity] = try context.fetch(fetchRequest)
            print("resultresult:\(result)")
            for person in result {
                UIImage.init(data: person.imageData!) //图片
            }
        } catch {
            let faError = error as NSError
            fatalError("Unresolved error \(faError), \(faError.userInfo)")
        }
    }
}
