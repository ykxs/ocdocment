//
//  CMPModel.swift
//  FirstSwiftCoreData
//
//  Created by ykxs on 2019/9/10.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit
import CoreMotion

class CMPModel: NSObject {
    
    let cmp = CMPedometer.init()
    class var sharedInstance :CMPModel {
        let instance = CMPModel()
        return instance
    }
    
    override init() {
        super.init()
        
        setContentData()
    }
    func setContentData() {
        if CMPedometer.isStepCountingAvailable() {
            let calendar = NSCalendar.current
            let now = Date.init()
            var components = calendar.dateComponents([.year,.month,.day,.hour,.minute,.second], from: now)
            components.hour = 0
            components.minute = 0
            components.second = 0
            let startDate = calendar.date(from: components)
            let endDate = calendar.date(byAdding: .day, value: 1, to: startDate!)
            cmp.queryPedometerData(from: startDate!, to: endDate!) {(pedData, error) in
                print("numberOfSteps:\(pedData!.numberOfSteps)")
            }
        }
    }
}
