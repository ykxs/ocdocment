//
//  HealthManager.swift
//  FirstSwiftCoreData
//
//  Created by ykxs on 2019/9/10.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit
import HealthKit

let HKVersion = UIDevice.current.systemVersion.hashValue

class HealthManager: NSObject {
    
    var healthStore :HKHealthStore?
    class var sharedInstance :HealthManager {
        let instance = HealthManager()
        return instance
    }
    
    func authorizeHealthKit(_ completion:@escaping(_ success:Bool) -> ()) {
        //        if HKVersion >= 8 {
        //            let error = NSError.init(domain: "com.raywenderlich.tutorials.healthkit", code: 2, userInfo: [NSLocalizedDescriptionKey:"HealthKit is not available in th is Device"])
        //            completion(false,error)
        //        }
        
        if HKHealthStore.isHealthDataAvailable() {
            if self.healthStore == nil {
                self.healthStore = HKHealthStore.init()
                let writeDataTypes = self.dataTypesToWrite()
                let readDataTypes = self.dataTypesRead()
                self.healthStore?.requestAuthorization(toShare:writeDataTypes, read: readDataTypes, completion: { (aSuccess, aError:Error!) in
                    print("aError:\(aError)")
                    completion(aSuccess)
                })
            }
        } else {
            let userInfo = ["iOS系统低于8.0":NSLocalizedDescriptionKey] // Dictionary.init(dictionaryLiteral: "iOS系统低于8.0",nslo)2
            let aError = NSError.init(domain: HKErrorDomain, code: 0, userInfo: userInfo)
            completion(false)
        }
    }
    func dataTypesToWrite() -> Set<HKSampleType>{
        let energyType = HKObjectType.quantityType(forIdentifier: .dietaryEnergyConsumed)
        let heightType = HKObjectType.quantityType(forIdentifier: .height)
        let weightType = HKObjectType.quantityType(forIdentifier: .bodyMass)
        let tempertureType = HKObjectType.quantityType(forIdentifier: .bodyTemperature)
        let activeEnergyType = HKObjectType.quantityType(forIdentifier: .activeEnergyBurned)
        return Set.init(arrayLiteral: energyType,heightType,weightType,tempertureType,activeEnergyType) as! Set<HKSampleType>
    }
    
    func dataTypesRead() -> Set<HKObjectType> {
        let energyType = HKObjectType.quantityType(forIdentifier: .dietaryEnergyConsumed)
        let heightType = HKObjectType.quantityType(forIdentifier: .height)
        let weightType = HKObjectType.quantityType(forIdentifier: .bodyMass)
        let tempertureType = HKObjectType.quantityType(forIdentifier: .bodyTemperature)
        let stepCountType = HKObjectType.quantityType(forIdentifier: .stepCount)
        let distance = HKObjectType.quantityType(forIdentifier: .distanceWalkingRunning)
        let activeEnergyType = HKObjectType.quantityType(forIdentifier: .activeEnergyBurned)
        let birthdayType = HKObjectType.characteristicType(forIdentifier: .dateOfBirth)
        let sexType = HKObjectType.characteristicType(forIdentifier: .biologicalSex)
        return Set.init(arrayLiteral: energyType,heightType,weightType,tempertureType,stepCountType,distance,activeEnergyType,birthdayType,sexType) as! Set<HKObjectType>
    }
    
    static func predicateForSamplesToday() -> NSPredicate {
        let calendar = Calendar.current
        let nowDate = Date.init()
        var components = calendar.dateComponents(Set.init(arrayLiteral: .year,.month,.day), from: nowDate)
        components.hour = 0
        components.minute = 0
        components.second = 0
        
        let startDate = calendar.date(from: components)
        let endDate = calendar.date(byAdding: .day, value: 1, to: startDate!)
        let predicate = HKQuery.predicateForSamples(withStart: startDate, end: endDate, options: HKQueryOptions.init())
        return predicate
    }
    
    //获取公里数
    func getDistance(_ completion:@escaping(_ value:Double,_ eeor:Error) -> Void) {
        let distanceType = HKObjectType.quantityType(forIdentifier: .distanceWalkingRunning)
        let timeSortDescriptor = NSSortDescriptor.init(key: HKSampleSortIdentifierEndDate, ascending: false)
        let query = HKSampleQuery.init(sampleType: distanceType!, predicate: HealthManager.predicateForSamplesToday(), limit: HKObjectQueryNoLimit, sortDescriptors: [timeSortDescriptor]) { (query:HKSampleQuery, results:[HKSample]?, aError:Error?) in
            if (aError != nil) {
                completion(0,aError!)
            } else {
                var totalSteps:Double = 0
                for quantitySample in results! {
                    let sample = quantitySample as! HKQuantitySample
                    let quantity = sample.quantity
                    let distanceUnit = HKUnit.meterUnit(with: .kilo)
                    let usersHeight = quantity.doubleValue(for: distanceUnit)
                    totalSteps += usersHeight
                }
                completion(totalSteps,aError!)
            }
        }
        self.healthStore?.execute(query)
    }
    
    func getStepCount(_ completion:@escaping(_ value:Double) -> Void) {
        let stepType = HKObjectType.quantityType(forIdentifier: .stepCount)
        let foodType = HKObjectType.correlationType(forIdentifier: .food)
        
        let timeSortDescriptor = NSSortDescriptor.init(key: HKSampleSortIdentifierEndDate, ascending: false)
        let query = HKSampleQuery.init(sampleType: foodType!, predicate: HealthManager.predicateForSamplesToday(), limit: HKObjectQueryNoLimit, sortDescriptors: [timeSortDescriptor]) { (query:HKSampleQuery, results:[HKSample]?, aError) in
            //            if (aError != nil) {
            //                completion(0,aError!)
            //            } else {
            var totalSteps:Double = 0
            for quantitySample in results! {
                let sample = quantitySample as! HKQuantitySample
                let quantity = sample.quantity
                let dheightUnit = HKUnit.meterUnit(with: .kilo)
                let usersHeight = quantity.doubleValue(for: dheightUnit)
                totalSteps += usersHeight
            }
            completion(totalSteps)
        }
        self.healthStore?.execute(query)
    }
}
