//
//  SlidingScrollView.swift
//  GreatMovie
//
//  Created by ykxs on 2019/9/19.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit

class SlidingScrollView: UIScrollView,UIScrollViewDelegate {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
//        setContentView()
    }
    
    //设置
    func setContentView() {
        
    }
    
    func setViewState() {
        self.showsVerticalScrollIndicator = false
        self.showsHorizontalScrollIndicator = false
        
        self.bounces = false //回弹效果关闭
        self.isPagingEnabled = true //分页
        self.delegate = self
    }
    
    //设置滑动的页码
    func setUpContentSize(_ count:Int, _ height:CGFloat) {
        setViewState()
        self.contentSize = CGSize(width: ScreenWidth * CGFloat(count), height: height)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        fatalError("init(coder:) has not been implemented")
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let offSet = self.contentOffset
        let page = offSet.x / ScreenHeight
        print("offff:\(offSet.x):\(offSet.y):\(page):\(ScreenWidth * page)")
        setContentOffset(CGPoint(x: ScreenHeight * page, y: offSet.y), animated: true)
    }
    
}
