//
//  MovieDetailVC.swift
//  GreatMovie
//
//  Created by ykxs on 2019/9/20.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit

var addBtnState = "addBtnState"
enum IsSelect:Int {
    case NoSelect         // 已收藏,可移除
    case Select      // 未收藏,可收藏     // 禁止收藏
}
class MovieDetailVC: RootVC {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var photoIv: UIImageView!
    
    @IBOutlet weak var cNameLa: UILabel!
    @IBOutlet weak var eNameLa: UILabel!
    @IBOutlet weak var otherLa: UILabel!
    
    @IBOutlet weak var firstBtn: UIButton!
    @IBOutlet weak var secondBtn: UIButton!
    @IBOutlet weak var thirdBtn: UIButton!
    @IBOutlet weak var fourthBtn: UIButton!
    
    @IBOutlet weak var contentLa: UILabel!
    
    @IBOutlet weak var bgView: UIView!
    
    var buttonArr:[UIButton] = []
    
    @IBOutlet weak var headerBgView: UIView!
    
    @IBOutlet weak var tabView: UITableView!
    
    var lineLa = UILabel()
    
    var baseY:CGFloat = -111.0
    
    var movieId = ""
    var detailModel:MoiveDetailModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        buttonArr.append(firstBtn)
        buttonArr.append(secondBtn)
        buttonArr.append(thirdBtn)
        buttonArr.append(fourthBtn)
        
        lineLa.frame = CGRect(x: 0, y: firstBtn.frame.maxY + 2, width: 65, height: 2)
        lineLa.backgroundColor = UIColor.red
        headerBgView.addSubview(lineLa)
        
        tabView.register(UINib(nibName: "MovieDetailCell", bundle: nil), forCellReuseIdentifier: "cell")
//        tabView.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: "cell")
//        tabView.register(MovieDetailHeaderView.classForCoder(), forHeaderFooterViewReuseIdentifier: "headerView")
//        let rect = tabView.frame
        tabView.backgroundColor = UIColor.green
//        tabView.frame = CGRect(x: 0, y: rect.minY, width: 300, height: ScreenWidth)
//        tabView.contentSize = CGSize(width: <#T##CGFloat#>, height: <#T##CGFloat#>)
//        tabView.frame.size = CGSize(width: 300, height: ScreenWidth)
        print("dahhdh:\(tabView.frame.size.height)")
        tabView.transform = tabView.transform.rotated(by: CGFloat(-Double.pi / 2))
        NetWorkTool.networkGetDetailMovieData("290", movieId) { (isSuccess, model) in
            if isSuccess {
                self.detailModel = model
                self.setHeaderViewData()
                self.tabView.reloadData()
            }
        }
    }
    
    //头部视图的数据加载
    func setHeaderViewData() {
        var typeStr = ""
        for str in self.detailModel!.type {
            typeStr.append("\(str)/")
        }
        otherLa.text = "\(typeStr)\(self.detailModel!.runTime)/\(self.detailModel!.release["date"] ?? "")\n\(self.detailModel!.release["location"] ?? "")上映"
        photoIv.setImageUrlStr(self.detailModel!.image)
        cNameLa.text = self.detailModel?.titleCn
        eNameLa.text = self.detailModel?.titleEn
        cNameLa.backgroundColor = UIColor.red
        let cSize = self.detailModel?.content.boundingRect(with: CGSize(width: ScreenWidth - 30, height: 0), options: NSStringDrawingOptions(rawValue: NSStringDrawingOptions.truncatesLastVisibleLine.rawValue|NSStringDrawingOptions.usesFontLeading.rawValue|NSStringDrawingOptions.usesLineFragmentOrigin.rawValue), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 15)], context: nil).size
//        self.contentLa.frame.size = CGSize(width: ScreenWidth - 30, height: cSize!.height + 5)
        self.contentLa.text = self.detailModel?.content
        self.contentLa.frame.size = CGSize(width: ScreenWidth - 30, height: cSize!.height + 5)
        self.bgView.backgroundColor = UIColor.green
        self.bgView.frame.size = CGSize(width: ScreenWidth, height: cSize!.height + self.headerBgView.frame.minY + 55)
//        self.scrollView.contentSize = CGSize(width: ScreenWidth, height: self.bgView.frame.maxY + 150 + NavTotalBarHeight)
        tabView.frame = CGRect(x: 0, y: self.bgView.frame.maxY + 50, width: ScreenWidth, height: 220)
        self.scrollView.contentSize = CGSize(width: ScreenWidth, height: self.tabView.frame.maxY + 150 + NavTotalBarHeight)
        print("cSize:\(self.tabView.frame.minY):\(self.tabView.frame.maxY)||\(self.bgView.frame.maxY + 150 + NavTotalBarHeight)||\(self.bgView.frame.maxY):\(ScreenHeight)")
        
    }
    
    //点击事件
    @IBAction func selectBtnDone(_ sender: UIButton) {
        
        setUpCenterAndFont(sender)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.setUpCenterAndFont(firstBtn)
    }
    
    func setUpCenterAndFont(_ button:UIButton) {
        //xib有问题
        UIView.animate(withDuration: 0.2, animations: {
            self.lineLa.center = CGPoint(x: button.center.x, y: self.lineLa.center.y)
        })
        for bu in self.buttonArr {
            if button.tag == bu.tag {
                bu.setTitleColor(UIColor.green, for: .normal)
                bu.titleLabel?.font = UIFont.systemFont(ofSize: 16)
            } else {
                bu.setTitleColor(UIColor.blue, for: .normal)
                bu.titleLabel?.font = UIFont.systemFont(ofSize: 14)
            }
        }
    }
}

//协议
extension MovieDetailVC:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        print("zouad")
        return 130
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MovieDetailCell
        cell.iv.setImageUrlStr("http:img5.mtime.cn/mt/2019/08/20/184519.87782615_1280X720X2.jpg")
//        cell.iv.isHidden = true
        cell.iv.backgroundColor = UIColor.yellow
        if indexPath.row == 0 {
            print("kasihdia")
            cell.backgroundColor = UIColor.red
//            cell.nameLa.text = "dgsdga"
//            cell.directorLa.text = "dddd"
        }
        return cell
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
//        cell.nameLa.text = "dgsdga"
//        cell.directorLa.text = "dddd"
//        cell.iv.setImageUrlStr(self.detailModel!.image)
//        return cell!
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == tabView {
            if baseY == -111.0 {
                baseY = scrollView.contentOffset.y
            }
            if scrollView.contentOffset.y - baseY > 100 {
                
            }
        }
    }
}

extension UIImageView {
    func setImageUrlStr(_ urlStr:String) {
        kf.setImage(with: URL(string: urlStr))
//        kf.setImage(with: URL(string: urlStr), placeholder: nil, options: nil, progressBlock: nil) { (im, error, ca, ul) in
//            print("urlstr:\(im?.size.width):\(im?.size.height)")
//        }
        
    }
}
