//
//  MovieDetailCell.swift
//  GreatMovie
//
//  Created by ykxs on 2019/9/22.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit

class MovieDetailCell: UITableViewCell {

    @IBOutlet weak var iv: UIImageView!
    
    @IBOutlet weak var nameLa: UILabel!
    @IBOutlet weak var llLa: UILabel!
    @IBOutlet weak var directorLa: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.nameLa.text = "name"
        self.llLa.text = "llll"
        self.directorLa.text = "director"
        self.transform = self.transform.rotated(by: CGFloat(double_t.pi / 2))
//        self.iv.transform = self.iv.transform.rotated(by: CGFloat(double_t.pi / 2))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
