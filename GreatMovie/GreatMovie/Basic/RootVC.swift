//
//  RootVC.swift
//  GreatMovie
//
//  Created by ykxs on 2019/9/18.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit

class RootVC: UIViewController,UIGestureRecognizerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        let target = self.navigationController?.interactivePopGestureRecognizer?.delegate
        let panGes = UIPanGestureRecognizer(target: target, action: Selector(("handleNavigationTransition:")))
        panGes.delegate = self
       self.view.addGestureRecognizer(panGes)
       self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
//        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        
        setLeftBarButtonItem()
    }
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if self.classForCoder == ViewController.classForCoder() {
            return false
        }
        return true
    }
    
    
    //MARK:寻常自定义方法
    func setLeftBarButtonItem() {
        let leftBarBtnItem = UIBarButtonItem.init(image: UIImage(named: "left"), style: .done, target: self, action: #selector(backToUpVC))
//        leftBarBtnItem.tintColor = UIColor.red //不影响背景颜色
        self.navigationItem.leftBarButtonItem = leftBarBtnItem
    }
    
    func setTitleStringColorFont(_ title:String,_ color:UIColor,_ fontSize:CGFloat) {
        let titleLa = UILabel(frame: CGRect(x: 60, y: StateBarHeight, width: ScreenWidth - 120, height: NavBarHeight))
        titleLa.text = title;
        titleLa.textColor = color
        titleLa.font = UIFont.systemFont(ofSize: fontSize)
        self.view.addSubview(titleLa)
    }
    
    @objc func backToUpVC() {
        self.navigationController?.popViewController(animated: true)
    }
    
}
