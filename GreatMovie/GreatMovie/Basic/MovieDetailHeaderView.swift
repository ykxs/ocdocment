//
//  MovieDetailHeaderView.swift
//  GreatMovie
//
//  Created by ykxs on 2019/9/21.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit

class MovieDetailHeaderView: UITableViewHeaderFooterView {

    fileprivate var titleLa:UILabel?
    fileprivate var button:UIButton = {
        let button = UIButton(frame: CGRect(x: ScreenWidth - 100, y: 5, width: 85, height: 30))
//        button.addTarget(<#T##target: Any?##Any?#>, action: <#T##Selector#>, for: <#T##UIControl.Event#>)
        return button
    }()
    
    var model: [HotMoiveDetailModel]? {
        didSet {
            
        }
    }
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        setContentView()
    }
    
    func setContentView() {
        self.titleLa = UILabel(frame: CGRect(x: 15, y: 5, width: 100, height: 20))
        self.addSubview(self.titleLa!)
        
        self.addSubview(self.button)
    }
    
    func setData() {
        self.titleLa?.text = "title"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
