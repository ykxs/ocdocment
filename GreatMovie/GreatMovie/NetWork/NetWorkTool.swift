//
//  NetWorkTool.swift
//  GreatMovie
//
//  Created by ykxs on 2019/9/11.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct NetWorkTool {
    static func networkGetNoticeListData(pageCurrent:String,pageLimit:String,completionHandler:@escaping (_ isSuccess :Bool,_ newsList: [NSObject]) -> ()) {
        
    }
    //获取城市
    static func networkGetAllCityData(completionHandler:@escaping (_ isSuccess :Bool,_ newsList: [CityModel]?) -> ()) {
        Alamofire.request(All_City, method: .get, parameters: nil, headers: nil).responseData { (response) in
            guard response.result.isSuccess else {
                print("response.result.error:\(String(describing: response.result.error))")
                return }
            if let value = response.result.value {
                let json = JSON(value)
                guard let data = json.dictionaryObject else { return }
                completionHandler(true,PPModel.deserialize(from: data)!.p)
            } else {
                completionHandler(false,nil)
            }
        }
    }
    
    //正在售票
    static func networkGetSalingMovieListData(_ cityStr:String,completionHandler:@escaping (_ isSuccess :Bool,_ hotMovieList: [SalingTicketMovieModel]) -> ()) {
//        杭州
        Alamofire.request(Saling_Movie + cityStr, method: .get, parameters: nil, headers: nil).responseJSON(completionHandler: { (response) in
            
            if response.result.isSuccess {
                let json = JSON(response.data!)
                guard let data = json["movies"].array else {
                    return
                }
                completionHandler(true,data.compactMap({SalingTicketMovieModel.deserialize(from: $0.dictionaryObject)}))
            } else {
                completionHandler(false,[])
            }
        })
    }
    
    //热门电影
    static func networkGetHotMovieListData(_ cityStr:String,completionHandler:@escaping (_ isSuccess :Bool,_ hotMovieList: [HotMoiveDetailModel]) -> ()) {
        //        杭州
        Alamofire.request(Hot_Movie + cityStr, method: .get, parameters: nil, headers: nil).responseData { (response) in
            print("isSuccess:\(response.result.isSuccess):\(Hot_Movie + cityStr)")
            if response.result.isSuccess {
                let json = JSON(response.result.value as Any)
                guard let data = json["ms"].array else {
                    return
                }
                completionHandler(true,data.compactMap({HotMoiveDetailModel.deserialize(from: $0.dictionaryObject)}))
            }
        }
    }
    //即将上演
    static func networkGetUpComingMovieListData(_ cityStr:String,completionHandler:@escaping (_ isSuccess :Bool,_ hotMovieList: ComingMoiveModel) -> ()) {
        //        杭州
        Alamofire.request(Up_Coming_Movie + cityStr, method: .get, parameters: nil, headers: nil).responseJSON { (response) in
            if response.result.isSuccess {
                let json = JSON(response.data!)
                
                guard let data = json.dictionaryObject else {
                    return
                }
               print("isSuccess:\(data)")
                completionHandler(true,ComingMoiveModel.deserialize(from: data)!)
            } else {
                completionHandler(false,ComingMoiveModel())
            }
        }
    }
    //电影详情
    static func networkGetDetailMovieData(_ cityStr:String,_ idStr:String,completionHandler:@escaping (_ isSuccess :Bool,_ hotMovieList: MoiveDetailModel) -> ()) {
        //        杭州
        Alamofire.request(String(format: Detail_Movie, cityStr,idStr), method: .get, parameters: nil, headers: nil).responseJSON { (response) in
            let json = JSON(response.data!)
            print("isSuccess:\(json):\(String(format: Detail_Movie, cityStr,idStr))")
            if response.result.isSuccess {
                
                guard let data = json.dictionaryObject else {
                    return
                }
                completionHandler(true,MoiveDetailModel.deserialize(from: data)!)
            } else {
                completionHandler(false,MoiveDetailModel())
            }
        }
    }
    
    //演职员表217896
    static func networkGetMovieActorListData(_ movieIdStr:String,completionHandler:@escaping (_ isSuccess :Bool,_ hotMovieList: [MoiveActorModel]) -> ()) {
        //        杭州
        Alamofire.request(Actor_List_Movie + movieIdStr, method: .get, parameters: nil, headers: nil).responseJSON { (response) in
            if response.result.isSuccess {
                let json = JSON(response.data!)
                
                guard let data = json["types"].array else {
                    return
                }
                print("isSuccess:\(data)")
                completionHandler(true,data.compactMap({MoiveActorModel.deserialize(from: $0.dictionaryObject)}))
            } else {
                completionHandler(false,[])
            }
        }
    }
    //评论
    static func networkGetMovieCommentListData(_ movieIdStr:String,completionHandler:@escaping (_ isSuccess :Bool,_ hotMovieList: MoiveCriticModel) -> ()) {
        //        杭州
        Alamofire.request(Comment_Movie + movieIdStr, method: .get, parameters: nil, headers: nil).responseJSON { (response) in
            print("isSuccess:\(Comment_Movie + movieIdStr)")
            if response.result.isSuccess {
                let json = JSON(response.data!)
                print("isSuccess:\(json)")
                guard let data = json["data"].dictionaryObject else {
                    return
                }
                completionHandler(true,MoiveCriticModel.deserialize(from: data)!)
            } else {
                completionHandler(false,MoiveCriticModel())
            }
        }
    }
    //花絮
    static func networkGetTraviaMovieCommentListData(_ pageIndex:Int,_ movieIdStr:String,completionHandler:@escaping (_ isSuccess :Bool,_ hotMovieList: [MoiveDetailTraviaModel]) -> ()) {
        
        Alamofire.request(String(format: Travia_Movie, pageIndex,movieIdStr), method: .get, parameters: nil, headers: nil).responseJSON { (response) in
            print("isSuccess:\(Comment_Movie + movieIdStr)")
            if response.result.isSuccess {
                let json = JSON(response.data!)
//                print("isSuccess:\(json)")
                guard let data = json["videoList"].array else {
                    return
                }
                completionHandler(true,data.compactMap({MoiveDetailTraviaModel.deserialize(from: $0.dictionaryObject)}))
            } else {
                completionHandler(false,[])
            }
        }
    }
    //剧照
    static func networkGetPhotoMovieCommentListData(_ movieIdStr:String,completionHandler:@escaping (_ isSuccess :Bool,_ hotMovieList: MoivePhotoModel?) -> ()) {
        //        杭州
        Alamofire.request(Photo_Movie + movieIdStr, method: .get, parameters: nil, headers: nil).responseJSON { (response) in
//            print("isSuccess:\(response.data)")
            if response.result.isSuccess {
                let json = JSON(response.data!)
                print("isSuccess:\(json)")
                guard let data = json.dictionaryObject else {
                    return
                }
                completionHandler(true,MoivePhotoModel.deserialize(from: data)!)
            } else {
                completionHandler(false,nil)
            }
        }
    }
}
