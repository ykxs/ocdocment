//
//  NetWorkManger.swift
//  DesignDemo
//
//  Created by shen li on 2019/9/12.
//  Copyright © 2019 Shenzhen youvision technology co. LTD. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
//https://api-m.mtime.cn/Showtime/LocationMovies.api?locationId=290
protocol RequestProtocol {
    var host: String {get}
    var path: String {get}
    var params: Dictionary<String, Any>? {get}
    var method: HTTPMethod {get}
    var body:Data?{get}
}

enum NetWorkRequest:RequestProtocol {
    
    case getMovieList(id:String)
    case getHotWord
    
    var host: String {
        return "http://api-m.mtime.cn"
    }
    var path: String{
        switch self {
        case .getMovieList(_):
            return "/Showtime/LocationMovies.api"
        default:
            return ""
        }
    }
    var params: Dictionary<String, Any>? {
        switch self {
        case .getMovieList(let id):
            return ["locationId":id]
        default:
            return nil
        }
    }
    var method: HTTPMethod {
        return .get
    }
    var body: Data?{
        return nil
    }
    
}
extension NetWorkRequest {
    typealias CallBack = (_ response: Response) -> Void
    
    
    static let SessionManager: Alamofire.SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 10//超时时间
        let SessionManager = Alamofire.SessionManager(configuration: configuration)
        return SessionManager
    }()
    private func getRequestURL() ->URLRequest? {
        do {
            var request = try URLRequest.init(url: host + path, method: method)
            if method == .post {
                request.httpBody = body
                request = try URLEncoding.queryString.encode(request, with: params)
                print("postpost")
                
            }else{
                request = try URLEncoding.default.encode(request, with: params)
                print("getget")
            }
            return request
        } catch  {
            return nil
        }
    }
    func response(call:@escaping CallBack) {
        guard let urlReuest = getRequestURL() else {
            
            call(Response.requestError())
            return
        }
        debugPrint("RequestUrl:\(urlReuest.url?.description ?? "url 为空 ????")")
        let request = NetWorkRequest.SessionManager.request(urlReuest)
        
        request.responseJSON { (respon) in
            let responData = Response.init(response: respon)
            call(responData)
        }
        
    }
}

open class Response: NSObject {
    open var request: URLRequest?
    open var response: HTTPURLResponse?
    open var data: Any?
    open var error: Swift.Error?
    open var isSuccess: Bool {
        return data != nil && error == nil
    }
    
    override init() {
    }
    init(response: DataResponse<Any>) {
        
        self.request = response.request
        self.response = response.response
        
        switch response.result {
        case .success(let value):
            self.data = value
//            print("selll:\(self.data)")
            break
        case .failure(let error):
            self.error = error
            break
        }
        
    }
    static func requestError() -> Response {
        let response = Response()
        return response
    }
}
extension Response {
    func getMoviceList()->[LiMoviceModel] {
        
        guard let dict = data as? [String:Any] else {
            return []
        }
//        print("data:\(dic)")
        if let dictArray = dict["ms"] as? [[String:Any]] {
//            print("dcc:\(dictArray):\([SalingTicketMovieModel].deserialize(from: dictArray))")
            let items = Mapper<LiMoviceModel>().mapArray(JSONArray: dictArray)
            
            return items
        }
        
        return []
        
    }
//    情，有感而发；意，随心所指
}
