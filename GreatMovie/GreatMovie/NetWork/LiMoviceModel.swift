//
//  LiMoviceModel.swift
//  GreatMovie
//
//  Created by ykxs on 2019/9/14.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit
import ObjectMapper

class LiMoviceModel: Mappable {
    var t:String?
    var img:String?
    var commonSpecial:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        t   <- map["t"]
        img <- map["img"]
        commonSpecial <- map["commonSpecial"]
    }
    
    
}
