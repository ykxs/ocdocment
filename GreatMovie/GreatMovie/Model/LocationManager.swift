//
//  LocationManager.swift
//  GreatMovie
//
//  Created by ykxs on 2019/9/12.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit
import MapKit
import AMapLocationKit

typealias SuccessBlock = (Bool)->()
class LocationManager: NSObject,AMapLocationManagerDelegate {
    
    var location:CLLocation?
    var regeCode:AMapLocationReGeocode?
    
    var manager:AMapLocationManager?
    
    static let shared = LocationManager()
    
    override init() {
        super.init()
        
        AMapServices.shared()?.apiKey = ""
        manager = AMapLocationManager.init()
        manager?.delegate = self
    }
    
    func startLocationPosition(success:@escaping SuccessBlock) {
        self.manager?.desiredAccuracy = kCLLocationAccuracyHundredMeters
        self.manager?.locationTimeout = 2
        self.manager?.reGeocodeTimeout = 2
        self.manager?.requestLocation(withReGeocode: true, completionBlock: { (location, reGeocode, error) in
            if (error != nil) {
                success(false)
            } else {
                self.location = location
                success(true)
            }
        })
    }
//    func cLLocationDistance(_ Longitude:String,_ latitude:String,_ distacne:String) -> Bool {
//        return false
//    }
}
