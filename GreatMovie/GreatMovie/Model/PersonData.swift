//
//  PersonData.swift
//  GreatMovie
//
//  Created by ykxs on 2019/9/19.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit

@objcMembers class PersonData: NSObject {
    var name:String?
    var age:Int = 18
    var title = "I like league of legend"
    var a:String?
    class func propertyList() -> [String] {
        var count:UInt32 = 0
        let list = class_copyPropertyList(self, &count)
        print("属性的数量:\(count)")
        for i in 0..<Int(count) {
            
            let property = list![i]
            //属性的名字
            let cName = property_getName(property)
            let name = String(utf8String: cName)
            print("cName:\(cName)")
            print("name:\(name as Any)")
        }
        return []
    }
}
