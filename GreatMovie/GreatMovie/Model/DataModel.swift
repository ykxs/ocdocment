//
//  DataModel.swift
//  GreatMovie
//
//  Created by ykxs on 2019/9/11.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import Foundation
import HandyJSON

//MARK:获取所有的id
struct CtyModel:HandyJSON {
    var count = 0
    var id = 0
    var n = ""  //城市名字
    var pinyinFull = ""
    var totalHotMovie = ""
}

struct PModel:HandyJSON  {
    
    var p = [CityModel]()
}

class PPModel:NSObject,HandyJSON,NSCoding {
    
    var p = [CityModel]()
    
    override required init() {
        super.init()
    }
    
    init(dic:[String : Any]) {
        super.init()
        setValuesForKeys(dic)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(p, forKey: "p")
    }
    
    required init?(coder aDecoder: NSCoder) {
        p = aDecoder.decodeObject(forKey: "p") as! [CityModel]
    }
}


//MARK:获取所有的id
class CityModel:NSObject,HandyJSON,NSCoding {
    
    var count = ""
    var id = ""
    var n = ""  //城市名字
    var pinyinFull = ""
    var pinyinShort = ""
    
    override required init() {
        super.init()
    }
    
    init(dic:[String : Any]) {
        super.init()
        print("zoulema")
        setValuesForKeys(dic)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(count, forKey: "count")
        aCoder.encode(id, forKey: "id")
        aCoder.encode(n, forKey: "n")
        aCoder.encode(pinyinFull, forKey: "pinyinFull")
        aCoder.encode(pinyinShort, forKey: "pinyinShort")
    }
    
    required init?(coder aDecoder: NSCoder) {
        count = aDecoder.decodeObject(forKey: "count") as! String
        id = aDecoder.decodeObject(forKey: "id") as! String
        n = aDecoder.decodeObject(forKey: "n") as! String
        pinyinFull = aDecoder.decodeObject(forKey: "pinyinFull") as! String
        pinyinShort = aDecoder.decodeObject(forKey: "pinyinShort") as! String
    }
}

//MARK:正在售票的电影
struct SalingTicketModel:HandyJSON {
    var count = 0
    var movies = [SalingTicketMovieModel]()
    var totalCinemaCount = 0  //同城影院数量
    var totalComingMovie = 0  //即将上映影片数量
    var totalHotMovie = 0  //实际正在售票电影数量
}
struct SalingTicketMovieModel:HandyJSON {
    var actorName1 = ""
    var actorName2 = ""
    var btnText = ""
    var commonSpecial = ""
    var directorName = ""
    var img = ""
    var is3D = false
    var isDMAX = false
    var isFilter = false
    var isHot = false
    var isIMAX = false
    var isIMAX3D = false
    var isNew = false
    var length = 0
    var movieId = 0  //影片 id，需要提供给影片详情
    var nearestShowtime = SalingTicketMoiveTimeModel()
    var rDay = ""
    var rMonth = ""
    var rYear = ""
    var ratingFinal = ""
    var titleCn = ""
    var titleEn = ""
    var type = ""
    var wantedCount = 0
}

struct SalingTicketMoiveTimeModel:HandyJSON {
    var isTicket = false
    var nearestCinemaCount = 0
    var nearestShowDay = 0
    var nearestShowtimeCount = 0
}

//MARK:正在热映的电影
struct HotMoiveModel:HandyJSON {
    
    var bImg = ""
    var date = ""
    var lid = 0
    var ms = [HotMoiveDetailModel]()
    var newActivitiesTime = 0
    var totalComingMovie = 0
    var voucherMsg = ""
}

struct HotMoiveDetailModel:HandyJSON {
    var NearestCinemaCount = 0
    var NearestDay = 0
    var NearestShowtimeCount = 0
    var aN1 = ""
    var aN2 = ""
    var cC = 0
    var commonSpecial = ""
    var d = ""
    var dN = ""
    var def = 0
    var id = 0
    var img = ""
    var actors = ""
    var is3D = false
    var isDMAX = false
    var isFilter = false
    var isHot = false
    var isIMAX = false
    var isIMAX3D = false
    var isNew = false
    var isTicket = false
    var m = ""
    var p =  ""//[String]
    var r = ""
    var rc = ""
    var rd = ""
    var rsC = 0
    var sC = 0
    var t = ""
    var tCn = ""
    var tEn = ""
    var ua = 0
    var versions = [HotMoiveVersionModel]()
    var wantedCount = 0
    var movieId = 0
}

struct HotMoiveVersionModel:HandyJSON {
    
    var `enum` = ""
    var version = ""
}

//MARK:即将上映
struct ComingMoiveModel:HandyJSON {
    
    var attention = [ComingAllMoiveModel]() //最受欢迎的电影
    var moviecomings = [ComingAllMoiveModel]() //即将上映的电影
}

struct ComingAllMoiveModel:HandyJSON {
    
    var actor1 = ""
    var actor2 = ""
    var director = ""
    var id = 0
    var image = ""
    var isFilter = false
    var isTicket = false
    var isVideo = false
    var locationName = ""
    var rDay = ""
    var rMonth = ""
    var rYear = ""
    var releaseDate = ""
    var title = ""
    var type = ""
    var videoCount = 0
    var videos = [ComingDeMoiveModel]()
    var wantedCount = 0
}

struct ComingDeMoiveModel:HandyJSON {
    
    var hightUrl = ""
    var image = ""
    var length = 0
    var title = ""
    var url = ""
    var videoId = ""
}

//MARK:影片详情
struct MoiveDetailModel:HandyJSON {
//    var releaseDate = ""
//    var releaseArea = ""
//    var picUrl = ""
    
    var weekBoxOffice = ""
    var totalNominateAward = ""
    var isEggHunt = false
    var personCount = 0
    var videos = [ComingDeMoiveModel]()
    var year = ""
    var totalBoxOfficeUnit = ""
//    var style = {}?
    var totalBoxOffice = ""
    var isIMAX3D = false
    var wapUrl = ""
    var isDMAX = false
    var is3D = false
    var videoCount = 0
    var titleCn = ""
    var scoreCount = ""
    var weekBoxOfficeUnit = ""
    var rating = ""
    var type = [String]()
    var director = Dictionary<String, String>()
//    "directorName" : "罗永昌",
//    "directorId" : 902944,
//    "directorImg" : "http:\/\/img31.mtime.cn\/ph\/2016\/08\/24\/183225.10343379_1280X720X2.jpg",
//    "directorNameEn" : "Wing-cheong Law"
    var hotRanking:Float = 0
    var showDay = 0
    var images = [String]()
    var directors = [String]()
    var content = ""
    var runTime = ""
    var showCinemaCount = ""
    var totalWinAward = 0
    var titleEn = ""
//    var festivals =
    var imageCount = 0
    var isIMAX = false
    var image = ""
    var video = ""
    var isTicket = false
//    var awards = []
    var actors = [String]()
    var url = ""
    var videoId = ""
    var commonSpecial = ""
    var actorList = [MoiveDetailActorModel]()
    var release = Dictionary<String, String>()
    var showtimeCount = 0
}
//演员信息
struct MoiveDetailActorModel:HandyJSON {
    var actor = ""
    var roleImg = ""
    var actorImg = ""
    var actorId = ""
    var roleName = ""
    var actorEn = ""
}
//演员表
struct MoiveActorModel:HandyJSON {
    var persons = [MoiveActorPersonModel]()
    var typeNameEn = ""
    var typeName = ""
//    var personateCn = ""
//    var roleCover = []
}
//y演员描述
struct MoiveActorPersonModel:HandyJSON {
    var image = ""
    var name = ""
    var nameEn = ""
    var id = ""
    
    var personate = "" //角色名字
    var personateCn = ""
    var personateEn = ""
    var roleCover = "" //角色剧照
}

/*
 struct MoiveDetailModel:HandyJSON {
 var advertisement = MoiveDetailAdModel()
 var basic = MoiveDetailBasicModel()
 var boxOffice = MoiveDetailBoxModel()
 //    var live = 0 {}
 var related = MoiveDetailRelaModel()
 }
struct MoiveDetailAdModel:HandyJSON {
    var advList = [MoiveDetailAdListModel]()
    var count = 0
    var error = ""
    var success = false
}

struct MoiveDetailAdListModel:HandyJSON {
    var advTag = ""
    var endDate = 0
    var isHorizontalScreen = false
    var isOpenH5 = false
    var startDate = ""
    var tag = ""
    var type = ""
    var typeName = ""
    var url = ""
}

struct MoiveDetailBasicModel:HandyJSON {
    
    var actors = [MoiveDetailBasicActorModel]()
    var award = ""
    var commentSpecial = ""
    var community = ""
    var director = ""
    var festivals = "" //[]
    var hotRanking = 0
    var img = ""
    var is3D = false
    var isDMAX = false
    var isEggHunt = false
    var isFilter = false
    var isIMAX = false
    var isIMAX3D = false
    var isTicket = false
    
    var message = ""
    var mins = ""
    var movieId = ""
    var name = ""
    var nameEn = ""
    var personCount = 0
//    var quizGame = {}
    var releaseArea = ""
    var releaseDate = ""
    var showCinemaCount = 0
    var showDay = 0
    var showtimeCount = 0
    
    var stageImg = [MoiveDetailStateModel]()
    var story = ""
    var style = {}
//    {
//        "isLeadPage": 0,
//        "leadImg": "https://img2.mtime.cn/mg/.jpg",
//        "leadUrl": ""
//    }
    var totalNominateAward = 0
    var totalWinAward = 0
    var type = "" //[String]
    var url = ""
}

struct MoiveDetailBasicActorModel:HandyJSON {
    
    var actorId = 0
    var img = ""
    var name = ""
    var nameEn = ""
    var roleImg = ""
    var roleName = ""
}

//boxOffice 解说
struct MoiveDetailBoxModel:HandyJSON {
    
    var movieId = 0
    var ranking = 0
    var todayBox = 0
    var todayBoxDes = ""
    var todayBoxDesUnit = ""
    var totalBox = 0
    var totalBoxDes = ""
    var totalBoxUnit = ""
}

struct MoiveDetailBasicAwardModel:HandyJSON {
    
    var award = ""
    var img = ""
    var name = ""
    var nameEn = ""
    var roleImg = ""
    var roleName = ""
}
struct MoiveDetailDirectorModel:HandyJSON {
    
    var directorId = ""
    var img = ""
    var name = ""
    var nameEn = ""
}
//related
struct MoiveDetailRelaModel:HandyJSON {
    var goodsList = [MoiveDetailAdListModel]()
    var goodsCount = 0
    var relateId = 0
    var relatedUrl = ""
    var type = 0
}
*/

//:MARK 花絮
struct MoiveDetailTraviaModel:HandyJSON {
    
    var hightUrl = ""
    var title = ""
    var type = 0
    var image = ""
    var length = 0
    var sourceType = 0
    var id = ""
    var url = ""
}
//MARK 剧照
struct MoivePhotoModel:HandyJSON {
    var images = [MoivePhotoImageModel]()
    var imageTypes = [MoivePhotoTypesModel]()
}
struct MoivePhotoImageModel:HandyJSON {
    
    var imageSubtypeDes = ""
    var id = ""
    var approveStatus = 1
    var imageSubtype = 0
    var image = ""
    var type = 1
}
struct MoivePhotoTypesModel:HandyJSON {

    var type = 0
    var typeName = ""
}

//MARK:影片评论
struct MoiveCriticModel:HandyJSON {
    
    var mini = MoiveMiniCriticListModel() //短评
    var plus = MoiveMiniCriticListModel()
}

struct MoiveMiniCriticListModel:HandyJSON {
    var clientPublish = 0
    var list = [MoiveMiniCriticModel]() //短评
    var total = 0
}

struct MoiveMiniCriticModel:HandyJSON {
    
    var commentDate = 0
    var commentId = 0
    var content = ""
    var headImg = ""
    var img = ""
    var isHot = false
    var isPraise = false
    var locationName = ""
    var nickname = ""
    var praiseCount = 0
    var rating:Float = 0
    var replyCount = 0
    var userId = 0
    //plus
    var isWantSee = false
    var title = ""
}
//"totalPageCount": 1,
//"totalCount": 13,
//"videoList": [MoiveTrialModel]
//MARK:电影预告片
struct MoiveTrialModel:HandyJSON {
    
    var id = ""
    var url = ""
    var hightUrl = ""
    var image = ""
    var title = ""
    var type = 0
    var length = 0
}
//MARK:剧照
struct MoviePhotoModel {
    var id = 0
    var image = ""
    var type = 0
}
