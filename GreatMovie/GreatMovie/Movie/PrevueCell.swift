//
//  PrevueCell.swift
//  GreatMovie
//
//  Created by ykxs on 2019/10/17.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit
import SDWebImage

class PrevueCell: UITableViewCell {

    @IBOutlet weak var iconBtn: UIButton!
    @IBOutlet weak var actionLa: UILabel!
    @IBOutlet weak var messageLa: UILabel!
    @IBOutlet weak var upLa: UILabel!
    
    @IBOutlet weak var iv: UIImageView!
    
    var iconButton:UIButton?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.iconBtn.isHidden = true
        self.iconButton = UIButton(frame: CGRect(x: 15, y: 15, width: ScreenWidth - 30, height: 200))
//        self.iconButton
        self.addSubview(iconButton!)
        
    }
    
//    var model:MoiveDetailTraviaModel? {
//        didSet {
////            self.upLa.text = self.model.
////            self.messageLa.text = self.model
//
//            self.actionLa.text = self.model?.title
//            print("modelImage:\(self.model?.image ?? "")")
////            self.iconBtn.imageView?.kf.setImage(with: URL(string: self.model?.image ?? ""))
//            self.iconBtn.backgroundColor = UIColor.gray
//            self.iconBtn.kf.setImage(with: URL(string: self.model?.image ?? ""), for: .normal)
//        }
//    }
    
    func setContentModel(_ model:MoiveDetailTraviaModel) {
        self.actionLa.text = model.title
        self.iconBtn.backgroundColor = UIColor.gray
//        SDWebImageManager.shared.loadImage(with: URL(string: "http://img5.mtime.cn/mg/2017/01/12/140320.88371998_235X132X4.jpg"), options: .progressiveLoad, progress: nil) { (image, data, error, type, success, uurl) in
//            print("imageee:\(image ?? UIImage.init())")
//            self.iconBtn.imageView?.image = image;//setImage(image, for: .normal)
//        }
        self.iconBtn.kf.setImage(with: URL(string: "http://img5.mtime.cn/mg/2017/01/12/140320.88371998_235X132X4.jpg"), for: .normal)
        self.iv.isHidden = true
//        self.iv.kf.setImage(with: URL(string: "http://img5.mtime.cn/mg/2017/01/12/140320.88371998_235X132X4.jpg"))
//        self.iconButton?.sd_setImage(with: URL(string: "http://img5.mtime.cn/mg/2017/01/12/140320.88371998_235X132X4.jpg"), for: .normal, completed: nil)
//        self.iconBtn.kf.setImage(with: URL(string: "http://img5.mtime.cn/mg/2017/01/12/140320.88371998_235X132X4.jpg"), for: .normal)
        
        print("model:\(model)")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
