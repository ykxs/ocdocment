//
//  RecommendView.swift
//  GreatMovie
//
//  Created by ykxs on 2019/10/16.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import Foundation

class RecommendView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setContentView()
    }
    
    lazy var tableView : UITableView = {
        let tableView = UITableView(frame: self.frame, style: .grouped)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 114
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorColor = UIColor.clear
        tableView.register(UINib(nibName: "NewsCell", bundle: nil), forCellReuseIdentifier: "NewsCell")
        return tableView
    }()
    
    func setContentView() {
        self.addSubview(self.tableView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

extension RecommendView : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecommendCell") as! RecommendCell
        return cell
    }
    
}
