//
//  PrevueView.swift
//  GreatMovie
//
//  Created by ykxs on 2019/10/17.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit

class PrevueView: UIView {

    var dataArr:[MoiveDetailTraviaModel] = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.tableView)
        setContentView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setContentView() {
        
        //290
        NetWorkTool.networkGetTraviaMovieCommentListData(1, "217896") { (isSuccess, modelArr) in
            print("modelArr",modelArr)
            self.dataArr = modelArr
            
            self.tableView.reloadData()
        }
//        networkGetTraviaMovieCommentListData
    }
    
    fileprivate lazy var tableView:UITableView = {
        let tableView = UITableView(frame: self.frame, style: .grouped)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 100
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorColor = UIColor.clear
        //        tableView.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: "cell")
        tableView.register(UINib(nibName: "PrevueCell", bundle: nil), forCellReuseIdentifier: "PrevueCell")
        return tableView
    }()
}

extension PrevueView : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PrevueCell") as! PrevueCell
//        cell.model = self.dataArr[indexPath.row]
//        print("modelmodel:\(self.dataArr[indexPath.row])")
        cell.setContentModel(self.dataArr[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 350;
    }
}
