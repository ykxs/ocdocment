//
//  TabBarVC.swift
//  GreatMovie
//
//  Created by ykxs on 2019/9/19.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit

class TabBarVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
//        self.selectedIndex = 1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
}
