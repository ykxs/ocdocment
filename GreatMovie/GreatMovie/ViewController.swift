//
//  ViewController.swift
//  GreatMovie
//
//  Created by ykxs on 2019/9/11.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit

var addBtnStateKey = "addBtnStateKey"

enum StateNum:Int {
    case Collected         // 已收藏,可移除
    case UnCollected      // 未收藏,可收藏
    case DisCollected      // 禁止收藏
}
class ViewController: RootVC {

    var index = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        self.view.layer.masksToBounds
        print(StateBarHeight)
        let bu = UIButton.init(frame: CGRect(x: 100, y: 100, width: 100, height: 100))
        bu.backgroundColor = UIColor.green
        bu.addTarget(self, action: #selector(buDone(bu:)), for: .touchUpInside)
        self.view.addSubview(bu)
        
        print("arc4random_uniform:\((arc4random_uniform(3)))")
    }
    
    @objc func buDone(bu:UIButton) {
        index += 1
        
        if index % 3 == 0 {
//            let touchup =
            objc_getAssociatedObject(bu, &addBtnStateKey)
//            print("touchup:\(touchup!)")
        } else {
            if arc4random_uniform(3) == StateNum.Collected.rawValue {
                objc_setAssociatedObject(bu, &addBtnStateKey, StateNum.Collected, .OBJC_ASSOCIATION_RETAIN)
            } else if arc4random_uniform(3) == StateNum.UnCollected.rawValue {
                objc_setAssociatedObject(bu, &addBtnStateKey, StateNum.UnCollected, .OBJC_ASSOCIATION_RETAIN)
            } else {
                objc_setAssociatedObject(bu, &addBtnStateKey, StateNum.DisCollected, .OBJC_ASSOCIATION_RETAIN)
            }
        }
        //        self.navigationController?.pushViewController(CitySelectVC.init(), animated: true)
    }
}

extension UIButton {
    
}
