//
//  HomePageVC.swift
//  GreatMovie
//
//  Created by ykxs on 2019/9/19.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit

class HomePageVC: RootVC {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("hiiihi")
//        let button = UIButton(frame: CGRect(x: 100, y: 100, width: 100, height: 100))
//        button.addTarget(self, action: #selector(pushToNextViewController), for: .touchUpInside)
//        button.backgroundColor = UIColor.green
//        self.view.addSubview(button)
        
        let sc = SlidingScrollView.init(frame: self.view.frame)
        sc.setUpContentSize(3, ScreenHeight)
        self.view.addSubview(sc)
        
        //一个选择框的高度
        let view1 = HotMovieView.init(frame: CGRect(x: 0, y: 40, width: ScreenWidth, height: ScreenHeight - 40 - KTabBarHeight))
        //[weak self]
        view1.pushBlock = { (movieId:Int) -> Void in
            self.pushToDetailViewController(movieId)
        }
        view1.backgroundColor = UIColor.green
        sc.addSubview(view1)
        let view2 = UIView.init(frame: CGRect(x: ScreenWidth, y: 0, width: ScreenWidth, height: ScreenHeight))
        view2.backgroundColor = UIColor.yellow
        sc.addSubview(view2)
        let view3 = UIView.init(frame: CGRect(x: ScreenWidth * 2, y: 0, width: ScreenWidth, height: ScreenHeight))
        view3.backgroundColor = UIColor.red
        sc.addSubview(view3)
        
        NetWorkTool.networkGetHotMovieListData("290") {
            if $0 {
                view1.dataArr = $1
            }
        }
    }
    
    //MARK: 点击事件
    //添加跳转方法
    @objc func pushToNextViewController() {
        
        let pushVC = CitySelectVC.init()
//        self.tabBarController?.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(pushVC, animated: true)
        
    }
    
    @objc func pushToDetailViewController(_ movieId:Int) {
        let pushVC = MovieDetailVC.init()
        pushVC.movieId = "\(movieId)"
        print("pushToDetailViewController")
        self.navigationController?.pushViewController(pushVC, animated: true)
    }
}
