//
//  LoactionCityVC.swift
//  GreatMovie
//
//  Created by ykxs on 2019/9/18.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit

class LoactionCityVC: RootVC {

    var cityArr:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let flowLayOut = UICollectionViewFlowLayout.init()
        flowLayOut.estimatedItemSize = CGSize(width: 90, height: 50)
        flowLayOut.minimumLineSpacing = 5
        flowLayOut.minimumInteritemSpacing = 10 //列间距
        flowLayOut.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        let collectionView = UICollectionView.init(frame: self.view.frame, collectionViewLayout: flowLayOut)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "CitySelectCell", bundle: nil), forCellWithReuseIdentifier: "CitySelectCell")
        cityArr = ["xix","wasedrfgvbhjnkml","ff","dd"]
        self.view.addSubview(collectionView)
        print("collectionView:\(collectionView):\(collectionView.autoresizingMask)")
    }
}

extension LoactionCityVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cityArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CitySelectCell", for: indexPath) as! CitySelectCell
        cell.titleLa.text = cityArr[indexPath.row]
        cell.titleLa.backgroundColor = UIColor.green
        return cell
    }
    

}
