//
//  CitySelectVC.swift
//  GreatMovie
//
//  Created by ykxs on 2019/9/18.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit

let hotCityArr = ["北京","广州","上海","深圳","重庆","天津","长沙","成都","大连","哈尔滨","杭州","合肥","南昌","南京","青岛","沈阳","苏州","太原","武汉","厦门","西安","郑州"]

class CitySelectVC: RootVC {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var flowLayOut: UICollectionViewFlowLayout!
    @IBOutlet weak var cityTF: UITextField!
//    var cityTF:UITextField?
    
    var currentIP:IndexPath?
    var cityArr:[CityModel] = []
    var block:((_ model:CityModel) -> ())?
    var isSearch = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black
        self.setLeftBarButtonItem()
        self.setTitleStringColorFont("选择城市", UIColor.black, 18)
        flowLayOut.estimatedItemSize = CGSize(width: 90, height: 50) //必须要
        collectionView.register(UINib(nibName: "CitySelectCell", bundle: nil), forCellWithReuseIdentifier: "CitySelectCell")
        collectionView.register(UICollectionReusableView.classForCoder(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "headerView")
//        cityArr = ["xix","wasedrfgvbhjnkml","ff","dd"]
        print("city:\(StateBarHeight)")
        
        //再写一个搜索的
        getAllCityId()
    }
    
    func getAllCityId() {
//        print("pahtPath:",cityPath)
        if FileManager.default.fileExists(atPath: cityPath) {
            self.cityArr = (NSKeyedUnarchiver.unarchiveObject(withFile: cityPath) as! [CityModel])
            print("获取存储数据:\(self.cityArr)")
            collectionView.reloadData()
        } else {
            NetWorkTool.networkGetAllCityData {
                if $0 {
                    self.cityArr = $1!
                    NSKeyedArchiver.archiveRootObject($1!, toFile: cityPath)
                    self.cityArr = (NSKeyedUnarchiver.unarchiveObject(withFile: cityPath) as! [CityModel])
                    print("获取存储数据:\(self.cityArr)")
                    self.collectionView.reloadData()
                }
            }
        }
    }
}

extension CitySelectVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if isSearch {
            return 1
        }
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("ccell:\(cityArr.count)")
        if isSearch {
            return cityArr.count
        }
        return section == 0 ? hotCityArr.count : cityArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CitySelectCell", for: indexPath) as! CitySelectCell
//        if indexPath.row == 3 {
//            cell.backgroundColor = UIColor.yellow
//        }
        if !isSearch && indexPath.section == 0 {
            cell.titleLa.text = hotCityArr[indexPath.row]
        } else {
            let model = cityArr[indexPath.row]
            cell.titleLa.text = model.n
        }
//        cell.titleLa.backgroundColor = ColorFromHex(0x434343,1) //UIColor.0x434343
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader && isSearch {
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "headerView", for: indexPath)
            let titlela = UILabel(frame: CGRect(x: 10, y: 10, width: 140, height: 30))
            titlela.font = UIFont.systemFont(ofSize: 15, weight: .medium)
            titlela.text = indexPath.section == 0 ? "热门城市" : "所有城市"
            headerView.addSubview(titlela)
//            cityTF = UITextField(frame: CGRect(x: 160, y: 10, width: ScreenWidth - 165, height: 30))
//            cityTF!.borderStyle = .none
//            cityTF!.placeholder = "请选择城市"
//            headerView.addSubview(cityTF!)
            return headerView
        } else {
            return UICollectionReusableView.init()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return isSearch ? CGSize(width: 0, height: 0) : CGSize(width: ScreenWidth, height: 45) //section == 0 ? CGSize(width: 0, height: 0) : CGSize(width: ScreenWidth, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (currentIP != nil) && currentIP == indexPath {
            collectionView.reloadItems(at: [currentIP!])
        }
        currentIP = indexPath
        let cell = collectionView.cellForItem(at: currentIP!) as! CitySelectCell
        cell.setBgClolr()
        let model = cityArr[indexPath.row]
        cityTF?.text = model.n
        self.block!(model)
    }
}
