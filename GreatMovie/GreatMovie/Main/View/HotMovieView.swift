//
//  HotMovieView.swift
//  GreatMovie
//
//  Created by ykxs on 2019/9/20.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit

typealias PushBlock = (_ movieId:Int) -> Void

class HotMovieView: UIView {

    var pushBlock:PushBlock?
    fileprivate let headerDataArr = ["演职人员","预告片与花絮","剧照","短评","影评","热门话题","票房","扩展资料"]
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.tableView)
//        print("selframe:\(self.frame)")
    }
    
    var dataArr: [HotMoiveDetailModel]? {
        didSet {
            tableView.reloadData()
        }
    }
    fileprivate lazy var tableView:UITableView = {
        let tableView = UITableView(frame: self.frame, style: .plain)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 100
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorColor = UIColor.clear
//        tableView.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: "cell")
        tableView.register(UINib(nibName: "HotTabViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        return tableView
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setContentView() {
        
    }

}

extension HotMovieView:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArr?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! HotTabViewCell
        cell.setContentDataModel(self.dataArr![indexPath.row])
//        print("cellCoun:\(self.dataArr?.count)")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.dataArr![indexPath.row]
        self.pushBlock!(model.movieId)
    }
    
}
