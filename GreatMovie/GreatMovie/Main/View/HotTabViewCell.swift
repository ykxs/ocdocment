//
//  HotTabViewCell.swift
//  GreatMovie
//
//  Created by ykxs on 2019/9/20.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit

class HotTabViewCell: UITableViewCell {

    @IBOutlet weak var iv: UIImageView!
    @IBOutlet weak var nameLa: UILabel!
    @IBOutlet weak var contentLa: UILabel!
    @IBOutlet weak var starla: UILabel!
    
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var ticketBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }
    
    func setContentDataModel(_ model:HotMoiveDetailModel) {
        self.iv.kf.setImage(with: URL(string: model.img))
        self.nameLa.text = model.t
        self.contentLa.text = model.commonSpecial
        self.starla.text = model.actors //model.aN1 + "/\(model.aN2)"
        self.rateLabel.text = model.r
        if (model.r as NSString).floatValue < 0.1 {
            self.rateLabel.text = "暂无评分"
            self.rateLabel.adjustsFontSizeToFitWidth = true
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func ticketBtnDone(_ sender: UIButton) {
        
    }
}
