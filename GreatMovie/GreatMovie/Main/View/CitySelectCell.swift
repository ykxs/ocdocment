//
//  CitySelectCell.swift
//  GreatMovie
//
//  Created by ykxs on 2019/9/18.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit

class CitySelectCell: UICollectionViewCell {

    @IBOutlet weak var titleLa: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        titleLa.layer.cornerRadius = 5
        titleLa.layer.borderColor = UIColor.lightGray.cgColor
        titleLa.layer.borderWidth = 0.5
        titleLa.isUserInteractionEnabled = false
        
    }
    
    func setTitleText(text:String) {
        titleLa.text = text
        titleLa.textColor = UIColor.black
        titleLa.backgroundColor = UIColor.white
    }
    
    func setBgClolr() {
        titleLa.textColor = UIColor.white
        titleLa.backgroundColor = UIColor.red //UIColor(named: 0xFF5555)
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        
//        self.setNeedsLayout()
//        self.layoutIfNeeded()
//        let size = self.contentView.systemLayoutSizeFitting(layoutAttributes.size)
//        var cellFrame = layoutAttributes.frame
//        cellFrame.size.height = size.height
//        layoutAttributes.frame = cellFrame
//        return layoutAttributes
        
        let attributes = super.preferredLayoutAttributesFitting(layoutAttributes)
        var rect = self.titleLa.text!.boundingRect(with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: 35), options: NSStringDrawingOptions(rawValue: NSStringDrawingOptions.truncatesLastVisibleLine.rawValue|NSStringDrawingOptions.usesFontLeading.rawValue|NSStringDrawingOptions.usesLineFragmentOrigin.rawValue), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 14)], context: nil)
        rect.size.width += 30
        rect.size.height += 15
        attributes.frame = rect
        print("zoule:\(rect.size.width)")
        return attributes;
    }
}
