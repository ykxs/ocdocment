//
//  ApiDefine.swift
//  GreatMovie
//
//  Created by ykxs on 2019/9/11.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import Foundation
import Kingfisher

let BASE_URL = "https://api-m.mtime.cn/"
//获取所有的城市
let All_City = "\(BASE_URL)/Showtime/HotCitiesByCinema.api"
//正在上映
let Saling_Movie = "\(BASE_URL)PageSubArea/HotPlayMovies.api?locationId="
//热门电影
let Hot_Movie = "\(BASE_URL)Showtime/LocationMovies.api?locationId="
//即将上映
let Up_Coming_Movie = "\(BASE_URL)Movie/MovieComingNew.api?locationId="
//电影详情
let Detail_Movie = "\(BASE_URL)movie/detail.api?locationId=%@&movieId=%@"

//演职员表
let Actor_List_Movie = "\(BASE_URL)Movie/MovieCreditsWithTypes.api?movieId="
//评论
let Comment_Movie = "https://ticket-api-m.mtime.cn/movie/hotComment.api?movieId="
//花絮
let Travia_Movie = "\(BASE_URL)Movie/Video.api?pageIndex=%d&movieId=%@"
//剧照
let Photo_Movie = "\(BASE_URL)Movie/ImageAll.api?movieId="


