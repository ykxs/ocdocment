//
//  Const.swift
//  GreatMovie
//
//  Created by ykxs on 2019/9/11.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit
import RxSwift

// 屏幕的宽度
let ScreenWidth = UIScreen.main.bounds.width
/// 屏幕的高度
let ScreenHeight = UIScreen.main.bounds.height
//状态栏高度
let StateBarHeight = UIApplication.shared.statusBarFrame.size.height
//导航栏高度
let NavBarHeight:CGFloat = 44.0
//导航栏总高度
let NavTotalBarHeight = StateBarHeight + NavBarHeight
//底部TableBar的高度
let KTabBarHeight:CGFloat = StateBarHeight > 20 ? 83 : 49

//
let Standard = UserDefaults.standard

//城市存储的path
let cityPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last?.appending("/MovieCity.plist") ?? ""



//MARK:颜色
func RGB(_ r:Int, _ g:Int,_ b:Int ,_ a:CGFloat) -> UIColor {
    return RGBA(r, g, b, a)
}
func RGB(_ r:CGFloat, _ g:CGFloat,_ b:CGFloat ,_ a:CGFloat) -> UIColor {
    return RGBA(r, g, b, a)
}
func RGB(_ rgbValue:Int) -> UIColor {
    return ColorFromHex(rgbValue,1)
}

func RGBA(_ r:Int, _ g:Int,_ b:Int ,_ a:CGFloat) -> UIColor {
    return UIColor(red: CGFloat(r)/255.0, green: CGFloat(g)/255.0, blue: CGFloat(b)/255.0, alpha: a)
}
func RGBA(_ r:CGFloat, _ g:CGFloat,_ b:CGFloat ,_ a:CGFloat) -> UIColor {
    return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
}

func ColorFromHex(_ rgbValue: Int) -> UIColor {
    return UIColor(red: ((CGFloat)((rgbValue & 0xFF0000) >> 16)) / 255.0,
    green: ((CGFloat)((rgbValue & 0xFF00) >> 8)) / 255.0,
    blue: ((CGFloat)(rgbValue & 0xFF)) / 255.0,alpha: 1)
}

func ColorFromHex(_ rgbValue: Int,_ alpha: CGFloat) -> UIColor {
    
    return UIColor(red: ((CGFloat)((rgbValue & 0xFF0000) >> 16)) / 255.0,
                   green: ((CGFloat)((rgbValue & 0xFF00) >> 8)) / 255.0,
                   blue: ((CGFloat)(rgbValue & 0xFF)) / 255.0,alpha: alpha)
}
