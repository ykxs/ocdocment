//
//  TiketVC.swift
//  GreatMovie
//
//  Created by ykxs on 2019/9/29.
//  Copyright © 2019 1226042260@qq.con. All rights reserved.
//

import UIKit

class TiketVC: RootVC {

    @IBOutlet weak var cityBtn: UIButton!
    @IBOutlet weak var movieBtn: UIButton!
    @IBOutlet weak var threateBtn: UIButton!
    
    @IBOutlet weak var scrollView: SlidingScrollView!
    
    
    @IBOutlet weak var movieScrollView: SlidingScrollView!
    @IBOutlet weak var threateView: UIView!
    
//    let scrollHeight = scrollView.fr
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setContentSize()
        
        
    }
    
    func setContentSize() {
        
        scrollView.backgroundColor = UIColor.gray
        movieScrollView.backgroundColor = UIColor.blue
        let height = scrollView.frame.size.height
        
        scrollView.contentSize = CGSize(width: ScreenWidth * 2, height: height)
        movieScrollView.contentSize = CGSize(width: ScreenWidth * 2, height: height)
        
    }
    
    @IBAction func therateBtnDone(_ sender: UIButton) {
        
    }
    
    @IBAction func movieBtnDone(_ sender: UIButton) {
        
    }
    
    @IBAction func cityBtnDone(_ sender: UIButton) {
        let cityVC = CitySelectVC()
        cityVC.block = {
            print($0.id,$0.n)
        }
        self.navigationController?.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(cityVC, animated: true)
    }
    
    @IBAction func searchBtnDone() {
        
    }
    
    
    
}
